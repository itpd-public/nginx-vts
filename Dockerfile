FROM centos:centos7 as build
RUN cd /root && yum install rpmdevtools wget gcc unzip redhat-lsb-core openssl-devel zlib-devel pcre-devel -y && \
    wget http://nginx.org/packages/mainline/centos/7/SRPMS/nginx-1.15.0-1.el7_4.ngx.src.rpm && \
    rpm -Uhv nginx-1.15.0-1.el7_4.ngx.src.rpm && \
    wget https://github.com/vozlt/nginx-module-vts/archive/master.zip -O nginx-vts.zip && \
    unzip nginx-vts.zip && \
    rm -rf nginx-vts.zip && \
    sed -i '58s|")| --add-module=/root/nginx-module-vts-master")|' /root/rpmbuild/SPECS/nginx.spec && \
    rpmbuild -bb /root/rpmbuild/SPECS/nginx.spec 

FROM centos:centos7
LABEL maintainer="Maksim Golub <manefestoh@gmail.com>"
COPY --from=build /root/rpmbuild/RPMS/x86_64/nginx-1.15.0-1.el7_4.ngx.x86_64.rpm /tmp

RUN yum install -y wget openssl sed gettext &&\
    yum -y autoremove &&\
    yum clean all &&\
    rpm -ihv /tmp/nginx-1.15.0-1.el7_4.ngx.x86_64.rpm && \
    rm -rf /tmp/nginx-1.15.0-1.el7_4.ngx.x86_64.rpm && \
    chown -R nginx:nginx /var/cache/nginx

COPY ./nginx.conf /etc/nginx/
COPY ./default.conf /etc/nginx/conf.d/

RUN nginx -v

RUN ln -sf /dev/stdout /var/log/nginx/access.log && ln -sf /dev/stderr /var/log/nginx/error.log

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
